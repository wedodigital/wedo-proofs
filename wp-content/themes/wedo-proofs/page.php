<?php get_header(); ?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>
			<section class="header-grad">
				<svg class="landing-flood">
					<defs>
						<linearGradient id="greenGradient">
				            <stop offset="0" stop-color="#a8e063">
				            	<animate attributeName="stop-color" values="#a8e063;#f83600;#49a09d;#6a3093;#f46b45;#34e89e;#f4c4f3;#ee0979;#a8e063;" dur="30s" repeatCount="indefinite" />
				            </stop>
				            <stop offset="100%" stop-color="#56ab2f">
				            	<animate attributeName="stop-color" values="#56ab2f;#fe8c00;#5f2c82;#a044ff;#eea849;#0f3443;#fc67fa;#ff6a00;#56ab2f;" dur="30s" repeatCount="indefinite" />
				            </stop>
				        </linearGradient>
					</defs>
					<rect width="100%" height="100%" fill= "url(#greenGradient)" />
				</svg>
			</section>

			<section class="sitemap-overlay">
				<?php if(get_field('site_proofs')) { ?>
					<!--
						<div class="thumbs">
							<section class="container">
								<ul id="proof-thumbs">
									<?php while(the_repeater_field('site_proofs')) { ?>
										<?php $thumb = wp_get_attachment_image_src(get_sub_field('page_design'), 'thumbnail' ); ?>
										<li><a href=""><img src="<?php echo $thumb[0]; ?>" alt="<?php the_sub_field('page_title'); ?>"></a></li>
									<?php } ?>
								</ul>
							</section>
						</div>
					-->
					<ul class="proofs">
						<?php while(the_repeater_field('site_proofs')) { ?>
							<?php $full = wp_get_attachment_image_src(get_sub_field('page_design'), 'full' ); ?>
							<li>
								<div class="large-proof">
									<section class="browser-bar">
										<ul>
											<li></li>
											<li></li>
											<li></li>
										</ul>
										<div class="address-bar">
											<p><?php the_sub_field('page_title'); ?> <a href="<?php echo $full[0]; ?>" target="_blank"><i class="fa fa-external-link"></i> View full size</a></p>
										</div>
									</section>
									<a href="<?php echo $full[0]; ?>" target="_blank"><img src="<?php echo $full[0]; ?>" alt="<?php the_sub_field('page_title'); ?>"></a>
								</div>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>
				<div class="footer-w"></div>
			</section>

		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
