	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
	
	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
